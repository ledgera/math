package math

import (
	"sort"
	"strings"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"
)

type twoSlices struct {
	Positions []int
	Values    []float64
}

func (t twoSlices) Len() int {
	return len(t.Positions)
}

func (t twoSlices) Swap(i, j int) {
	t.Positions[i], t.Positions[j] = t.Positions[j], t.Positions[i]
	t.Values[i], t.Values[j] = t.Values[j], t.Values[i]
}

func (t twoSlices) Less(i, j int) bool {
	return t.Positions[i] < t.Positions[j]
}

// PlotBarChart ...
func PlotBarChart(filename, yLabel, xLabel string, positions []int, vals []float64, color int) error {
	values := plotter.Values(vals)

	p := plot.New()

	p.Title.Text = filename
	p.Y.Label.Text = yLabel
	p.X.Label.Text = xLabel

	w := vg.Points(20)

	// sort
	newTwoSlices := twoSlices{
		Positions: positions,
		Values:    values,
	}
	sort.Sort(newTwoSlices)

	barsA, err := plotter.NewBarChart(plotter.Values(newTwoSlices.Values), w)
	if err != nil {
		return err
	}

	barsA.LineStyle.Width = vg.Length(1)
	barsA.Color = plotutil.Color(color)

	p.Add(barsA)
	// p.NominalX(newTwoSlices.Observers...)

	if !strings.HasSuffix(filename, ".png") {
		filename = filename + ".png"
	}

	if err := p.Save(16*vg.Inch, 10*vg.Inch, filename); err != nil {
		return err
	}

	return nil
}
