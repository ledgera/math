package math

import (
	"math/big"
)

// HelicalPrimes --
type HelicalPrimes struct {
	primes         []int
	positionCounts map[int]int
	increment      bool
	position       int
}

func (h *HelicalPrimes) toggleDirection() {
	if h.increment {
		h.increment = false
		return
	}

	h.increment = true
}

func (h *HelicalPrimes) movePosition() {
	if h.increment {
		h.position++
	} else {
		h.position--
	}
}

func (h *HelicalPrimes) isPrime(value int) bool {
	for _, p := range h.primes {
		if value%p == 0 {
			return false
		}
	}

	if value > 1 {
		h.primes = append(h.primes, value)
	}

	return true
}

// NewHelixOfLength --
func NewHelixOfLength(length int) *HelicalPrimes {
	h := &HelicalPrimes{
		primes:         make([]int, 0),
		positionCounts: make(map[int]int),
		increment:      true,
		position:       0,
	}

	for i := 1; i < length; i++ {
		_, ok := h.positionCounts[h.position]
		if !ok {
			h.positionCounts[h.position] = 0
		}

		if big.NewInt(int64(i)).ProbablyPrime(20) {
			h.positionCounts[h.position]++
			h.toggleDirection()
		}

		h.movePosition()
	}

	return h
}
