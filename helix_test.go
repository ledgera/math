package math

import (
	"fmt"
	// "strconv"
	"testing"
)

func TestHelixPrimes(t *testing.T) {
	length := 1000000
	h := NewHelixOfLength(length)

	positions := make([]int, 0)
	counts := make([]float64, 0)
	for position, count := range h.positionCounts {
		if count > 0 {
			positions = append(positions, position)
			counts = append(counts, float64(count))

			fmt.Printf("%d-%d\n", position, count)
		}
	}

	filename := fmt.Sprintf("helical_primes_%d", length)
	err := PlotBarChart(filename, "count", "position", positions, counts, 1)
	if err != nil {
		t.Error(err)
	}
}
